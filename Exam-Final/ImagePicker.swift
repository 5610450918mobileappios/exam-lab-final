//
//  ImagePicker.swift
//  Exam-Final
//
//  Created by iOS Dev on 12/1/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import Foundation
import UIKit
enum MediaType{
    case IMAGE
    case MOVIE
    case UNKNOWN
}
class MediaSelected{
    var image:UIImage!
    var fileURL:NSURL!
    var mediaType:MediaType = MediaType.UNKNOWN
    
    init(){}
    
}
