//
//  ViewController.swift
//  Exam-Final
//
//  Created by iOS Dev on 12/1/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import UIKit
import MobileCoreServices

class ViewController: UIViewController {
    var mediaList:[MediaSelected] = [MediaSelected]()
    var pageViewController:PageViewController!
    var imageView:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "toPageViewControllerSegue"){
            let destination = segue.destinationViewController as! PageViewController
            destination.mediaList = self.mediaList
            self.pageViewController = destination
        }
    }
    func addNewPage(media:MediaSelected){
        if let vc = self.pageViewController{
            mediaList.insert(media, atIndex: vc.currentPage)
            vc.mediaList = mediaList
            let index = vc.currentPage
            if let controller = vc.createControllerAtIndex(index){
                vc.startingIndex = index
                vc.setViewControllers([controller], direction: .Forward, animated: false, completion: nil)
            }
        }
        else{ mediaList.append(media)}
    }
    
    @IBAction func unwindHome(seg:UIStoryboardSegue!){
        
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let mediaSelected = MediaSelected()
        
        let imageType = kUTTypeImage as String
        
        if let type = info[UIImagePickerControllerMediaType]{
            switch(type as! String){
            case imageType:
                mediaSelected.mediaType = MediaType.IMAGE
                mediaSelected.image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
                imageView.image = mediaSelected.image
                break;
            default:break;
            }
            addNewPage(mediaSelected)
        }
        dismissViewControllerAnimated(true, completion: nil)
    }


}

