//
//  PageImagePickerViewController.swift
//  Exam-Final
//
//  Created by iOS Dev on 12/1/2558 BE.
//  Copyright © 2558 iOS Dev. All rights reserved.
//

import UIKit
import MobileCoreServices

class PageImagePickerViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func addImageButtonTouch() {
        imagePicker.allowsEditing = false
        imagePicker.mediaTypes = [kUTTypeImage as String]
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let mediaSelected = MediaSelected()
        
        let imageType = kUTTypeImage as String
        
        if let type = info[UIImagePickerControllerMediaType]{
            switch(type as! String){
            case imageType:
                mediaSelected.mediaType = MediaType.IMAGE
                mediaSelected.image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
                imageView.image = mediaSelected.image
                break;
            default:break;
            }
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let vc = segue.destinationViewController as! ViewController
        vc.imageView = self.imageView
    }
    
    @IBAction func addImage(sender: AnyObject) {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
